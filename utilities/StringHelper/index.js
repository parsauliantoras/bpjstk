import joinClassName from './joinClassName.js';
import join from './join.js';
import upper from './upper.js';

const StringHelper = {
    join,
    joinClassName,
    upper,
};

export default StringHelper;
