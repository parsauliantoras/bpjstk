import { Roboto } from 'next/font/google';
import { createTheme } from '@mui/material/styles';
import tailwindConfig from '@/tailwind.config';
import resolveConfig from 'tailwindcss/resolveConfig';

const roboto = Roboto({
    weight: ['300', '400', '500', '700'],
    subsets: ['latin'],
    display: 'swap',
});

const tailwindTheme = resolveConfig(tailwindConfig).theme;

const theme = createTheme({
    palette: {
        mode: 'light',
        // primary: tailwindTheme?.colors?.primary as object,
        // secondary: tailwindTheme?.colors?.secondary as object,
        success: tailwindTheme?.colors?.green as object,
        warning: tailwindTheme?.colors?.yellow as object,
        error: tailwindTheme?.colors?.red as object,
        info: tailwindTheme?.colors?.blue as object,
        grey: tailwindTheme?.colors?.gray as object,
    },
    typography: {
        fontFamily: roboto.style.fontFamily,
    },
    components: {
        MuiAlert: {
            styleOverrides: {
                root: ({ ownerState }) => ({
                    ...(ownerState.severity === 'info' && {
                        backgroundColor: '#60a5fa',
                    }),
                }),
            },
        },
    },
});

export default theme;
