import './style.css';
import Link from 'next/link';

function NavMenu() {
    return (
        <nav className="app__footer__nav-menu">
            <ul className="app__footer__nav-menu__list">
                <li>
                    <Link href="/about">Tentang Kami</Link>
                </li>
                <li>
                    <Link href="/contact">Kontak</Link>
                </li>
            </ul>
        </nav>
    )
};

export default NavMenu;