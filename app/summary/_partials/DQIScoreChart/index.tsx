'use client'
import { SegmentationChartProps } from './type';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import HighchartsTreemap from 'highcharts/modules/treemap';
import segmentationChartConfig from './config/DQIScoreChart.config';
import useSize from '@/hooks/useSize';

if (typeof Highcharts === 'object') {
    HighchartsTreemap(Highcharts);
}

function SegmentationChart({ items }: SegmentationChartProps) {
    const [sizes1, ref1] = useSize<HTMLDivElement>();

    items = items?.map((item) => ({
        ...item,
        name: item.segment,
        value: item.CUSTBE,
    })) || [];

    return (
        <div
            ref={ref1}
            className="w-full"
        >
            <HighchartsReact
                highcharts={Highcharts}
                options={segmentationChartConfig({
                    data: items,
                    sizes: sizes1,
                })}
            />
        </div>
    );
}

export default SegmentationChart;
