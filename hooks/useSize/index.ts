import { MutableRefObject, useEffect, useRef, useState } from 'react';

function useSize<T> () {
    const [result, setResult] = useState<
        [Object, MutableRefObject<Extract<T, HTMLElement> | null>]
    >([{}, useRef(null)]);

    useEffect(() => {
        if (result[1]?.current) {
            new ResizeObserver((entries) => {
                setResult([
                    {
                        height: entries[0].target.clientHeight,
                        width: entries[0].target.clientWidth,
                    },
                    result[1],
                ]);
            }).observe(result[1].current);
        }
    }, []);

    return result;
};

export default useSize;
