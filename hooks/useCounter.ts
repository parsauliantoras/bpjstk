import { useRef } from 'react';

type UseCounterReturn = [number, (increment: number) => number];

function useCounter(initialValue: number = 0): UseCounterReturn {
    const counter = useRef(initialValue);

    function counterHandle(increment: number) {
        counter.current += increment;
        return counter.current;
    }

    return [counter.current, counterHandle];
}

export default useCounter;
